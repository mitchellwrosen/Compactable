# Compactable

This is a generalization of catMaybes, that has no dependencies on other abstractions. It is similar to Data.Witherable in that `traverseMaybe = wither`. It differs in that many instances work with Compactable that do not work with Witherable, as Compactable does not depend on Traversable or any other class.

Thanks to Morgan Thomas, Ryan Trinkle, and Edward Kmett for informing the
thinking of this library.
